{
  description = "kor";

  outputs = { self }:

  {
    strok = {
      djenereicyn = 10;
      spici = "lamdaIndeks";
    };

    datom = let
      inherit (builtins) attrNames hasAttr getAttr
      listToAttrs concatStringsSep  foldl' elem
      length elemAt head tail filter concatMap sort
      lessThan fromJSON toJSON readFile toFile
      typeOf isAttrs;

    in rec {
      nameValuePair = name: value: { inherit name value; };

      genAttrs = names: f:
        listToAttrs (map (n: nameValuePair n (f n)) names);

      optional = cond: elem: if cond then [elem] else [];
      optionals = cond: elems: if cond then elems else [];

      foldl = foldl';

      optionalString = cond: string: if cond then string else "";

      concatMapStringsSep = sep: f: list:
        concatStringsSep sep (map f list);

      optionalAttrs = cond: set: if cond then set else {};

      hasAttrByPath = attrPath: e:
        let attr = head attrPath;
        in
          if attrPath == [] then true
          else if e ? ${attr}
          then hasAttrByPath (tail attrPath) e.${attr}
          else false;

      mapAttrs' = f: set:
        listToAttrs (map (attr: f attr set.${attr}) (attrNames set));

      invertValueName = set: mapAttrs' (n: v: nameValuePair "${v}" n) set;

      filterAttrs = pred: set: listToAttrs (concatMap (name:
      let v = set.${name};
      in if pred name v then [(nameValuePair name v)] else []) (attrNames set));

      remove = e: filter (x: x != e);

      unique = list:
      if list == [] then [] else
      let x = head list;
      in [x] ++ unique (remove x list);

      flattenNV = list: map (x: x.value) list;

      mapAttrsToList = f: attrs:
        map (name: f name attrs.${name}) (attrNames attrs);

      attrsToList = attrs: map (a: attrs.${a}) (attrNames attrs);

      attrToNamedList = attrs: 
        mapAttrsToList (name: value: value // { inherit name; }) attrs;

      matchEnum = enum: match:
        genAttrs enum (name: name == match);

      louestOf = yrei: head (sort lessThan yrei);

      importJSON = path: fromJSON (readFile path);

      eksportJSON = neim: datom: toFile neim (toJSON datom);

      mkIf = condition: content:
      { _type = "if";
        inherit condition content;
      };

      mesydj = pred: msj:
      if pred then true
      else builtins.trace msj false;

      hazSingylAttr = attrs: (length (attrNames attrs)) == 1;

      izNioSpici = datom: isAttrs datom && hazSingylAttr datom;

      mkNioSpici = nioSpici @{ datom, spek }:
      assert mesydj (izNioSpici datom)
      "Datom ${datom} not structured like nioSpici";
      let
        neim = head (attrNames nioSpici.datom);
        spiciDatom = nioSpici.datom.${neim};
        elymyntNeimz = attrNames spiciDatom;
        spiciSpek = spek.${neim};
        datom = spiciSpek // spiciDatom;
        spekElymynts = attrNames spiciSpek;
        izKyrekt = elymyntNeim:
        let
          spiciType = typeOf spiciDatom.${elymyntNeim};
          spekType = typeOf spiciSpek.${elymyntNeim};
        in
        mesydj (elem elymyntNeim spekElymynts)
        "Elymynt ${elymyntNeim} not in spek"
        &&
        mesydj (spiciType == spekType)
        "Elymynt ${elymyntNeim} should be ${spiciSpek} but is ${spiciType}";
      in
        assert (builtins.all izKyrekt elymyntNeimz);
        { inherit neim datom; };

      cortHacString = string:
      builtins.substring 0 7
      (builtins.hashString "sha1" string);

      cortHacIuniks = iuniks:
      builtins.substring 0 7
      (builtins.hashFile "sha1" iuniks);

      spiciDatum = { datum, spek }:
      let
        inherit (datum) spici;
        allSpeksNeimz = concatMap (n: getAttr n spek) (attrNames spek);
        wantedAttrsNeimz = spek.${spici};
        izyntWanted = n: !(elem n wantedAttrsNeimz);
        unwantedAttrs = filter izyntWanted allSpeksNeimz;
      in
        removeAttrs datum unwantedAttrs;

      arkSistymMap = {
        x86-64 = "x86_64-linux";
        i686 = "i686-linux";
        aarch64 = "aarch64-linux";
        armv7l = "armv7l-linux";
        avr = "avr-none";
      };

    };
  };
}
